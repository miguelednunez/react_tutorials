const square = function (x) {
  return x * x;
}

const squareArrowOne = (x) => {
  return x * x;
};


const squareArrowTwo = (x) => x * x;


console.log(square(10));
console.log(squareArrowOne(10));
console.log(squareArrowTwo(10));

const getFirstNameOne = name => name.split(' ')[0];
const getFirstNameTwo = name => {
  return name.split(' ')[0]
};

console.log(getFirstNameOne('Miguel Nunez'));
console.log(getFirstNameTwo('Miguel Nunez'));
