// Arguments object - no longer bound with arrow funtions

const add = function (a, b) {
  
  return a + b;
};
console.log(add(55, 1, 1001));

// This keyword - no longer bound

let user = {
  name: 'Mike',
  cities: ['Philadelphia', 'New York', 'Dublin'],
  printPlacesLived: function() {
    const that = this;
    console.log(this.name);
    console.log(this.cities);

    this.cities.forEach(function(city) {
      console.log(that.name + ' wants to visit ' + city);
    });
  }
};
console.log(user.printPlacesLived());



user = {
  name: 'Mike',
  cities: ['Philadelphia', 'New York', 'Dublin'],
  printPlacesLived: function() {
    console.log(this.name);
    console.log(this.cities);

    this.cities.forEach((city) => {
      console.log(this.name + ' wants to visit ' + city);
    });
  }
};
console.log(user.printPlacesLived());


user = {
  name: 'Mike',
  cities: ['Philadelphia', 'New York', 'Dublin'],
  printPlacesLived() {
    console.log(this.name);
    console.log(this.cities);

    this.cities.forEach((city) => {
      console.log(this.name + ' wants to visit ' + city);
    });
  }
};
console.log(user.printPlacesLived());


user = {
  name: 'Mike',
  cities: ['Philadelphia', 'New York', 'Dublin'],
  printPlacesLived() {
    const cityMessages = this.cities.map((city) => {
      return this.name + ' wants to visit ' + city;
    });
    return cityMessages;
  }
};
console.log(user.printPlacesLived());

user = {
  name: 'Mike',
  cities: ['Philadelphia', 'New York', 'Dublin'],
  printPlacesLived() {
    return this.cities.map((city) => this.name + ' wants to visit ' + city);
  }
};
console.log(user.printPlacesLived());


const multiplier = {
  numbers: [1, 2, 3, 4],
  multiplyBy: 4,
  multiply() {
    return this.numbers.map((number) => this.multiplyBy * number);
  }
}
console.log(multiplier.multiply());