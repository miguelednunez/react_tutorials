// import './utils.js'
import subtract, { square, add } from './utils.js';
import isSenior, { isAdult, canDrink } from './person.js';

console.log('app.js is running');
console.log(square(2));
console.log(add(4, 6));
console.log('Is age 7 adult? ' + isAdult(7));
console.log('Is age 19 adult? ' + isAdult(19));
console.log('Is age 16 acceptable to start drinking? ' + canDrink(16));
console.log('Is Senior ' + isSenior(65));
