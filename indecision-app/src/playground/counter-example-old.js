class Counter extends React.Component {
  constructor(props) {
    super(props); 
    this.handleAddOne = this.handleAddOne.bind(this);
    this.handleMinusOne = this.handleMinusOne.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.state = {
      count: 0,
      name: 'John'
    };
  }
  handleAddOne() {
    this.setState((prevState) => {
      return {
        // count: this.state.count += 1,
        count: prevState.count + 1,
      }
    });
  } 
  
  handleMinusOne() {
    this.setState((prevState) => {
      return {
        // count: this.state.count -= 1,
        count: prevState.count - 1,
      };
    });
  } 
  
  handleReset() {
    this.setState(() => {
      return {
        count: 0,
      }
    });
  }

  render() {
    return (
      <div>
        {this.state.name}
        <h1>
          Count: {this.state.count}
        </h1>
        <button onClick={this.handleAddOne}>+1</button>
        <button onClick={this.handleMinusOne}>-1</button>
        <button onClick={this.handleReset}>reset</button>
      </div>
    )
  }
}


ReactDOM.render(<Counter />, document.getElementById('app'));


/**************
 * Old version
**************/

/*
console.log('App.js is running!');

// JSX - Javascript XML

const app = {
  title: 'indecision app',
  subtitle: 'Hello',
  options: ['Option1', 'Option2'],
}

function getSubtitle(subtitle) {
  if (subtitle) {
    return <p>subtitle</p>;
  }
}

const template = (
  <div>
    <h1>{app.title}</h1> 
    <p>{app.subtitle}</p>
    {app.subtitle && <p>{app.subtitle}</p>}
    <p>{(app.options.length > 0 ? "Here are your options" : "No options")}</p>
  </div>
);

function getLocation(location) {
  if (location) {
    return location;
  }
}

const user = {
  name: '',
  age: '28',
  location: 'Almancil',
};

const templateTwo = (
  <div>
    <h1>{user.name? user.name : 'Name missing' }</h1>
    {(user.age && user.age >= 18) && <p>{user.age}</p>}
    {getLocation(user.location)}
  </div>
);

let count = 0;
const addOne = () => {
  console.log("Add One");
  count++;
  renderCounterApp();
}

const minusOne = () => {
  console.log("Minus One");
  count--;
  renderCounterApp();
}

const reset = () => {
  console.log("Values Reset");
  count = 0;
  renderCounterApp();
}


const templateThree = (
  <div>
    <h1>
      Count: {count}
    </h1>
    <button onClick={addOne} className="button">+1</button>
    <button onClick={minusOne} className="button">-1</button>
    <button onClick={reset} className="button">reset</button>
  </div>
);
console.log(templateTwo);

const appRoot = document.getElementById('app')

const renderCounterApp = () => {
  const templateFour = (
    <div>
      <h1>
        Count: {count}
      </h1>
      <button onClick={addOne} className="button">+1</button>
      <button onClick={minusOne} className="button">-1</button>
      <button onClick={reset} className="button">reset</button>
    </div>    
  );

  ReactDOM.render(templateFour, appRoot);
};

// var1(template) -> Says what will be inserted in the html element, var2(appRoot)
ReactDOM.render(templateThree, appRoot);

*/