# React Tutorials

## Walktrough

1 - Download project

2 - Install npm dependencies with 
```
npm install
```

3 - Run the project with
```
npm run dev-server
```

4 - Go to http://127.0.0.1:8080/

