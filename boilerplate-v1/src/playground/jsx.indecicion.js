console.log("App.js is running");

const app = {
  title : 'Indecision App',
  subtitle: 'Put your life in the hands of a computer',
  options: ['One', 'Two']
};

const onFormSubmit = (e) => {
  e.preventDefault();
  const option = e.target.elements.option.value;

  if (option) {
    app.options.push(option);
    e.target.elements.option.value = '';
    formRender();
  }
}

const clearOptions = () => {
  app.options = [];
  console.log("Options Cleared");
  formRender();
}

const onMakeDecision = () => {
  const randomNum = Math.floor(Math.random() * app.options.length);
  console.log(randomNum);
  const option = app.options[randomNum];
  alert(option);
};

const formRender = () => {
  const template = (
    <div>
      <h1>{app.title}</h1>
      {app.subtitle && <p>{app.subtitle}</p>}
      <p>{app.options.length > 0 ? 'Here are your options': 'No options'}</p>
      <button disabled={app.options.length === 0} onClick={onMakeDecision}>What shoud  I do?</button>
      <button onClick={clearOptions}>Clear</button>
      <ol>
      {
        app.options.map((option) => {
          return <li key={option}>{option}</li>
        })
      }
      </ol>
      <form onSubmit={onFormSubmit}>
        <input type="text" name="option"/>
        <button>Add Option</button>
      </form>
    </div>
  );
  ReactDOM.render(template, appRoot);
};

const appRoot = document.getElementById('app');
// ReactDOM.render(template, appRoot);
formRender();