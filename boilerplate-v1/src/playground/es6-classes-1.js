
class Person {
   constructor(name = 'Anonymous', age = 0) {
    this.name = name;
    this.age = age;
   }
   getGreeting() {
     // return 'Hi. I am ' + this.name + '!';
     return `Hi. I am ${this.name}!`;
   }
   getDescription() {
     return `${this.name} is ${this.age} year(s) old.`;
   }
}

class Student extends Person {
  constructor(name, age, major) {
    super(name, age);
    this.major = major;
  }
  hasMajor() {
    return !!this.major;
  }
  getDescription() {
    let description = super.getDescription();
    if (this.major) {
      description += ` Their major is ${this.major}.`;
    }
    return description;
  }
}

class Traveler extends Person {
  constructor(name, age, homeLocation) {
    super(name, age);
    this.homeLocation = homeLocation;
  }

  getGreeting() {
    let gretting = super.getGreeting();
    if (this.homeLocation) {
      greeting += ` I'm visiting from ${this.homeLocation}.`;
    }
    return greeting;
  }
}



const me = new Person('John Doe', 28);
// console.log(me.getGretting());
// console.log(me.getDescription());

const other = new Person();
// console.log(other.getGretting());

const student = new Student('Mary Ann', 30, 'Computer Science');
// console.log(student.getDescription());
// console.log(student.hasMajor());

const otherStudent = new Student('John Cale');
// console.log(otherStudent.getDescription());
// console.log(otherStudent.hasMajor());


const traveler = new Traveler('Mike Nunez', 28, 'Faro');
console.log(traveler.getGreeting());

const otherTraveler = new Traveler();
console.log(otherTraveler.getGreeting());
