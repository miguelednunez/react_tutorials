// export const isAdult = (x) => x >= 18 ? true : false;

const isAdult = (x) => x >= 18 ? true : false;
const canDrink = (x) => x >= 16 ? true : false;
const isSenior = (age) => age >= 65 ? true : false;

export { isAdult, canDrink, isSenior as default}
